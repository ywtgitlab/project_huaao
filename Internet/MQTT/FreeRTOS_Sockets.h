#ifndef _FREERTOS_SOCKETS_H_
#define _FREERTOS_SOCKETS_H_

#define MQTT_TASK

typedef struct sockaddr{
	uint32_t sin_port;
	uint32_t sin_addr;
}freertos_sockaddr;


#endif
