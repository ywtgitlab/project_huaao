/*******************************************************************************
 * Copyright (c) 2014, 2015 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation and/or initial documentation
 *    Ian Craggs - convert to FreeRTOS
 *******************************************************************************/

#include "MQTTFreeRTOS.h"
#include <string.h>
#include "socket.h"
#include "w5500.h"

SemaphoreHandle_t send_xSemaphore = NULL;
SemaphoreHandle_t recv_xSemaphore = NULL;

int ThreadStart(Thread* thread, void (*fn)(void*), void* arg)
{
	int rc = 0;
	uint16_t usTaskStackSize = (configMINIMAL_STACK_SIZE * 5);
	UBaseType_t uxTaskPriority = uxTaskPriorityGet(NULL) - 1; /* set the priority as the same as the calling task*/ //add -1

	rc = xTaskCreate(fn,	/* The function that implements the task. */
		"MQTTTask",			/* Just a text name for the task to aid debugging. */
		usTaskStackSize,	/* The stack size is defined in FreeRTOSIPConfig.h. */
		arg,				/* The task parameter, not used in this case. */
		uxTaskPriority,		/* The priority assigned to the task is defined in FreeRTOSConfig.h. */
		&thread->task);		/* The task handle is not used. */

	return rc;
}


void MutexInit(Mutex* mutex)
{
	mutex->sem = xSemaphoreCreateMutex();
}

int MutexLock(Mutex* mutex)
{
	return xSemaphoreTake(mutex->sem, portMAX_DELAY);
}

int MutexUnlock(Mutex* mutex)
{
	return xSemaphoreGive(mutex->sem);
}


void TimerCountdownMS(Timer* timer, unsigned int timeout_ms)
{
	timer->xTicksToWait = timeout_ms / portTICK_PERIOD_MS; /* convert milliseconds to ticks */
	vTaskSetTimeOutState(&timer->xTimeOut); /* Record the time at which this function was entered. */
}


void TimerCountdown(Timer* timer, unsigned int timeout) 
{
	TimerCountdownMS(timer, timeout * 1000);
}


int TimerLeftMS(Timer* timer) 
{
	xTaskCheckForTimeOut(&timer->xTimeOut, &timer->xTicksToWait); /* updates xTicksToWait to the number left */
	return (timer->xTicksToWait < 0) ? 0 : (timer->xTicksToWait * portTICK_PERIOD_MS);
}


char TimerIsExpired(Timer* timer)
{
	return xTaskCheckForTimeOut(&timer->xTimeOut, &timer->xTicksToWait) == pdTRUE;
}


void TimerInit(Timer* timer)
{
	timer->xTicksToWait = 0;
	memset(&timer->xTimeOut, '\0', sizeof(timer->xTimeOut));
}


int FreeRTOS_read(Network* n, unsigned char* buffer, int len, int timeout_ms)
{
	TickType_t xTicksToWait = timeout_ms / portTICK_PERIOD_MS; /* convert milliseconds to ticks */
	TimeOut_t xTimeOut;
	int recvLen = 0;

	xSemaphoreTake(recv_xSemaphore, portMAX_DELAY);

	vTaskSetTimeOutState(&xTimeOut); /* Record the time at which this function was entered. */

	//printf("xTicksToWait: %d\r\n", xTicksToWait);
	do
	{
		int rc = 0;

		//FreeRTOS_setsockopt(n->my_socket, 0, FREERTOS_SO_RCVTIMEO, &xTicksToWait, sizeof(xTicksToWait));
		//rc = FreeRTOS_recv(n->my_socket, buffer + recvLen, len - recvLen, 0);
		rc = recv(n->my_socket, buffer + recvLen, len - recvLen);
		if (rc > 0)
			recvLen += rc;
		else if (rc < 0)
		{
			recvLen = rc;
			break;
		}
	} while (recvLen < len && xTaskCheckForTimeOut(&xTimeOut, &xTicksToWait) == pdFALSE);
	//printf("xTicksToWait: %d\r\n", xTicksToWait);

	xSemaphoreGive(recv_xSemaphore);

	return recvLen;
}



int FreeRTOS_write(Network* n, unsigned char* buffer, int len, int timeout_ms)
{
	TickType_t xTicksToWait = timeout_ms / portTICK_PERIOD_MS; /* convert milliseconds to ticks */
	TimeOut_t xTimeOut;
	int sentLen = 0;

	xSemaphoreTake(send_xSemaphore, portMAX_DELAY);

	vTaskSetTimeOutState(&xTimeOut); /* Record the time at which this function was entered. */
	do
	{
		int rc = 0;

		//FreeRTOS_setsockopt(n->my_socket, 0, FREERTOS_SO_RCVTIMEO, &xTicksToWait, sizeof(xTicksToWait));
		//rc = FreeRTOS_send(n->my_socket, buffer + sentLen, len - sentLen, 0);
		rc = send(n->my_socket, buffer + sentLen, len - sentLen);
		if (rc > 0)
			sentLen += rc;
		else if (rc < 0)
		{
			sentLen = rc;
			break;
		}
	} while (sentLen < len && xTaskCheckForTimeOut(&xTimeOut, &xTicksToWait) == pdFALSE);

	xSemaphoreGive(send_xSemaphore);

	return sentLen;
}


void FreeRTOS_disconnect(Network* n)
{
	int ret;
	//FreeRTOS_closesocket(n->my_socket);
	if((ret=disconnect(n->my_socket)) != SOCK_OK);// return ret;
	//close(n->my_socket);
}


void NetworkInit(Network* n)
{
	n->my_socket = 0;
	n->mqttread = FreeRTOS_read;
	n->mqttwrite = FreeRTOS_write;
	n->disconnect = FreeRTOS_disconnect;
}

#define _MQTT_TCP_DEBUG_
uint32_t dns_run(uint8_t* Domain_name);
int NetworkConnect(Network* n, char* ipAddress, int port)
{
	int ret;
	int retVal = -1;
	uint8_t addr[4];

//	if ((ipAddress = FreeRTOS_gethostbyname(addr)) == 0)
//		goto exit;

//	socket(n->my_socket, Sn_MR_TCP, myport, 0);

//	connect(n->my_socket, addr, port);

//	if (retVal = connect(n->my_socket, ipAddress, port) <= 0)
//	{
////		FreeRTOS_closesocket(n->my_socket);
//		disconnect(n->my_socket);
//	    goto exit;
//	}

   *(uint32_t*)addr = dns_run((uint8_t*)ipAddress);
//   addr[0] = 10;
//   addr[1] = 0;
//   addr[2] = 0;
//   addr[3] = 78;

   uint16_t any_port = 	5000;

   while(1)
   {

	   switch(getSn_SR(n->my_socket))
	   {
		  case SOCK_ESTABLISHED :
			 if(getSn_IR(n->my_socket) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
			 {
	#ifdef _MQTT_TCP_DEBUG_
				printf("%d:MQTT Connected to - %d.%d.%d.%d : %d\r\n",n->my_socket, addr[0], addr[1], addr[2], addr[3], port);
	#endif
				setSn_IR(n->my_socket, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
			 }
			 retVal = 0;
			 goto exit;
			 break;

		  case SOCK_CLOSE_WAIT :
	#ifdef _MQTT_TCP_DEBUG_
			 printf("%d:MQTT CloseWait\r\n",n->my_socket);
	#endif
			 if((ret=disconnect(n->my_socket)) != SOCK_OK) return ret;
	#ifdef _MQTT_TCP_DEBUG_
			 printf("%d:MQTT Socket Closed\r\n", n->my_socket);
	#endif
			 break;

		  case SOCK_INIT :
	#ifdef _MQTT_TCP_DEBUG_
			 printf("%d:MQTT Try to connect to the %d.%d.%d.%d : %d\r\n", n->my_socket, addr[0], addr[1], addr[2], addr[3], port);
	#endif
			 if( (ret = connect(n->my_socket, (uint8_t *)addr, port)) != SOCK_OK) retVal=ret;	//	Try to TCP connect to the TCP server (destination)
			 break;

		  case SOCK_CLOSED:
			  close(n->my_socket);
			  if((ret=socket(n->my_socket, Sn_MR_TCP, any_port++, 0x00)) != n->my_socket) retVal=ret; // TCP socket open with 'any_port' port number
	#ifdef _MQTT_TCP_DEBUG_
			 printf("%d:MQTT TCP client start\r\n",n->my_socket);
			 printf("%d:MQTT Socket opened\r\n",n->my_socket);
	#endif
			 break;
		  default:
			 break;
	   }
   }

exit:
	return retVal;
}


#if 0
int NetworkConnectTLS(Network *n, char* addr, int port, SlSockSecureFiles_t* certificates, unsigned char sec_method, unsigned int cipher, char server_verify)
{
	SlSockAddrIn_t sAddr;
	int addrSize;
	int retVal;
	unsigned long ipAddress;

	retVal = sl_NetAppDnsGetHostByName(addr, strlen(addr), &ipAddress, AF_INET);
	if (retVal < 0) {
		return -1;
	}

	sAddr.sin_family = AF_INET;
	sAddr.sin_port = sl_Htons((unsigned short)port);
	sAddr.sin_addr.s_addr = sl_Htonl(ipAddress);

	addrSize = sizeof(SlSockAddrIn_t);

	n->my_socket = sl_Socket(SL_AF_INET, SL_SOCK_STREAM, SL_SEC_SOCKET);
	if (n->my_socket < 0) {
		return -1;
	}

	SlSockSecureMethod method;
	method.secureMethod = sec_method;
	retVal = sl_SetSockOpt(n->my_socket, SL_SOL_SOCKET, SL_SO_SECMETHOD, &method, sizeof(method));
	if (retVal < 0) {
		return retVal;
	}

	SlSockSecureMask mask;
	mask.secureMask = cipher;
	retVal = sl_SetSockOpt(n->my_socket, SL_SOL_SOCKET, SL_SO_SECURE_MASK, &mask, sizeof(mask));
	if (retVal < 0) {
		return retVal;
	}

	if (certificates != NULL) {
		retVal = sl_SetSockOpt(n->my_socket, SL_SOL_SOCKET, SL_SO_SECURE_FILES, certificates->secureFiles, sizeof(SlSockSecureFiles_t));
		if (retVal < 0)
		{
			return retVal;
		}
	}

	retVal = sl_Connect(n->my_socket, (SlSockAddr_t *)&sAddr, addrSize);
	if (retVal < 0) {
		if (server_verify || retVal != -453) {
			sl_Close(n->my_socket);
			return retVal;
		}
	}

	SysTickIntRegister(SysTickIntHandler);
	SysTickPeriodSet(80000);
	SysTickEnable();

	return retVal;
}
#endif
