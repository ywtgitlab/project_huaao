/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "fsl_dspi.h"
#include "fsl_dspi_freertos.h"

#include "freertos_w5500_dspi.h"
#include "board.h"

#include "wizchip_conf.h"
#include "dhcp.h"
#include "dns.h"
#include "loopback.h"
#include "sntp.h"

#if ((defined FSL_FEATURE_SOC_INTMUX_COUNT) && (FSL_FEATURE_SOC_INTMUX_COUNT))
#include "fsl_intmux.h"
#endif

#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
* Definitions
******************************************************************************/

#define ETH_DSPI_MASTER_BASE (SPI0)
#define ETH_DSPI_MASTER_IRQN (SPI0_IRQn)
#define ETH_DSPI_CLK_SRC (DSPI0_CLK_SRC)
#define ETH_DSPI_MASTER_PCS_FOR_INIT kDSPI_Pcs0
#define ETH_DSPI_MASTER_PCS_FOR_TRANSFER kDSPI_MasterPcs0

//#define ETH_TRANSFER_SIZE (256)         /*! Transfer size */
#define ETH_TRANSFER_BAUDRATE (5000000U) /*! Transfer baudrate - 5000k */

/*******************************************************************************
* Variables
******************************************************************************/
#define MY_MAX_DHCP_RETRY			2
uint8_t my_dhcp_retry = 0;

dspi_rtos_handle_t      eth_master_rtos_handle;
TimerHandle_t 			Dhcp1sTimerHandle = NULL;
/*******************************************************************************
 * Definitions
 ******************************************************************************/
/* Interrupt priorities. */
#if (__CORTEX_M >= 0x03)
#define DSPI_NVIC_PRIO 5
#else
#define DSPI_NVIC_PRIO 2
#endif
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void Dhcp1sTimerCallback(TimerHandle_t xTimer)
{
	DHCP_time_handler();
	DNS_time_handler();
}

void  wizchip_select(void)
{
//	Chip_GPIO_SetPinState(LPC_GPIO, 0, 2, false);	// SSEL(CS)
	ETH_CS_SELECT();
}

void  wizchip_deselect(void)
{
//	Chip_GPIO_SetPinState(LPC_GPIO, 0, 2, true);	// SSEL(CS)
	ETH_CS_DESELECT();
}

uint8_t wizchip_read()
{
	uint8_t rb;
	dspi_transfer_t masterXfer;
//	Chip_SSP_ReadFrames_Blocking(LPC_SSP0, &rb, 1);
    /* Start master transfer */
    masterXfer.txData = NULL;
    masterXfer.rxData = &rb;
    masterXfer.dataSize = 1;
    masterXfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
    if( kStatus_Success != DSPI_RTOS_Transfer(&eth_master_rtos_handle, &masterXfer)) {
        PRINTF("rx_data failed\n");
        return false;
    }
	return rb;
}

void  wizchip_write(uint8_t wb)
{
//	Chip_SSP_WriteFrames_Blocking(LPC_SSP0, &wb, 1);
	dspi_transfer_t masterXfer;

    masterXfer.txData = &wb;
    masterXfer.rxData = NULL;
    masterXfer.dataSize = 1;
    masterXfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
    if( kStatus_Success != DSPI_RTOS_Transfer(&eth_master_rtos_handle, &masterXfer)){
        PRINTF("tx_data failed\n");
        return;
    }
}

void test_dspi()
{

	dspi_transfer_t masterXfer;
	uint8_t	wb;

	for (int i=0; i<5; i++)
	{
		masterXfer.txData = &wb;
		masterXfer.rxData = NULL;
		masterXfer.dataSize = 1;
		masterXfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
		if( kStatus_Success != DSPI_RTOS_Transfer(&eth_master_rtos_handle, &masterXfer)){
			PRINTF("tx_data failed\r\n");
			return;
		}else{
			PRINTF("tx_data successfully\r\n");
		}
	}
}

void SPI_Init()
{
	dspi_master_config_t    eth_masterConfig;
    uint32_t                eth_sourceClock;
    status_t                status;

    /*Master config*/
    eth_masterConfig.whichCtar = kDSPI_Ctar0;
    eth_masterConfig.ctarConfig.baudRate = ETH_TRANSFER_BAUDRATE;
    eth_masterConfig.ctarConfig.bitsPerFrame = 8;
    eth_masterConfig.ctarConfig.cpol = kDSPI_ClockPolarityActiveHigh;
    eth_masterConfig.ctarConfig.cpha = kDSPI_ClockPhaseFirstEdge;
    eth_masterConfig.ctarConfig.direction = kDSPI_MsbFirst;
    eth_masterConfig.ctarConfig.pcsToSckDelayInNanoSec = 2000;
    eth_masterConfig.ctarConfig.lastSckToPcsDelayInNanoSec = 2000;
    eth_masterConfig.ctarConfig.betweenTransferDelayInNanoSec = 2000;

    eth_masterConfig.whichPcs = kDSPI_Pcs0;
    eth_masterConfig.pcsActiveHighOrLow = kDSPI_PcsActiveLow;

    eth_masterConfig.enableContinuousSCK = false;		//continue transfer 2 burst
    eth_masterConfig.enableRxFifoOverWrite = false;
    eth_masterConfig.enableModifiedTimingFormat = false;
    eth_masterConfig.samplePoint = kDSPI_SckToSin0Clock;

    NVIC_SetPriority(SPI0_IRQn, 6);

    eth_sourceClock = CLOCK_GetFreq(ETH_DSPI_CLK_SRC);
    status = DSPI_RTOS_Init(&eth_master_rtos_handle, ETH_DSPI_MASTER_BASE, &eth_masterConfig, eth_sourceClock);

    if (status != kStatus_Success)
    {
        PRINTF("DSPI master: error during initialization. \r\n");
    }
    PRINTF("DSPI master: initialized. \r\n");

    //test_dspi();
}

void W5500_Init()
{
	uint8_t tmp;
	uint8_t memsize[2][8] = { { 2, 2, 2, 2, 2, 2, 2, 2 }, { 2, 2, 2, 2, 2, 2, 2, 2 } };

	wizchip_select();
	wizchip_deselect();
	ETH_nRST_SELECT();

	tmp = 0xFF;
	while(tmp--);//while(tmp--);while(tmp--);
	ETH_nRST_DESELECT();

	reg_wizchip_cs_cbfunc(wizchip_select, wizchip_deselect);
	reg_wizchip_spi_cbfunc(wizchip_read, wizchip_write);

	/* wizchip initialize*/
	if (ctlwizchip(CW_INIT_WIZCHIP, (void*) memsize) == -1) {
		PRINTF("WIZCHIP Initialized fail.\r\n");
		while (1);
	}

	do {
		if (ctlwizchip(CW_GET_PHYLINK, (void*) &tmp) == -1)
			PRINTF("Unknown PHY Link stauts.\r\n");
	} while (tmp == PHY_LINK_OFF);
}

wiz_NetInfo gWIZNETINFO = { //.mac = {0x00, 0x08, 0xdc, 0xab, 0x3d, 0xef},
							.mac = {0x00, 0x5a, 0x39, 0xe4, 0x86, 0x8f},
                            .ip =  {10, 0, 0, 111},
                            .sn =  {255, 255, 255, 0},
                            .gw =  {10, 0, 0, 1},
                            .dns = {10, 0, 0, 1},
                            .dhcp = NETINFO_DHCP/*NETINFO_STATIC*/ };

static void Display_Net_Conf()
{
	uint8_t tmpstr[6] = {0,};

	ctlnetwork(CN_GET_NETINFO, (void*) &gWIZNETINFO);

	// Display Network Information
	ctlwizchip(CW_GET_ID,(void*)tmpstr);

	if(gWIZNETINFO.dhcp == NETINFO_DHCP) PRINTF("\r\n===== %s NET CONF : DHCP =====\r\n",(char*)tmpstr);
		else PRINTF("\r\n===== %s NET CONF : Static =====\r\n",(char*)tmpstr);
	PRINTF(" MAC: %02X:%02X:%02X:%02X:%02X:%02X\r\n", gWIZNETINFO.mac[0], gWIZNETINFO.mac[1], gWIZNETINFO.mac[2], gWIZNETINFO.mac[3], gWIZNETINFO.mac[4], gWIZNETINFO.mac[5]);
	PRINTF(" IP : %d.%d.%d.%d\r\n", gWIZNETINFO.ip[0], gWIZNETINFO.ip[1], gWIZNETINFO.ip[2], gWIZNETINFO.ip[3]);
	PRINTF(" GW : %d.%d.%d.%d\r\n", gWIZNETINFO.gw[0], gWIZNETINFO.gw[1], gWIZNETINFO.gw[2], gWIZNETINFO.gw[3]);
	PRINTF(" SN : %d.%d.%d.%d\r\n", gWIZNETINFO.sn[0], gWIZNETINFO.sn[1], gWIZNETINFO.sn[2], gWIZNETINFO.sn[3]);
	PRINTF(" DNS: %d.%d.%d.%d\r\n", gWIZNETINFO.dns[0], gWIZNETINFO.dns[1], gWIZNETINFO.dns[2], gWIZNETINFO.dns[3]);
	PRINTF("=======================================\r\n");
}

void link_debug(void)
{
#define VER_H		1
#define VER_L		00
	uint8_t tmpstr[6] = {0,};

	ctlwizchip(CW_GET_ID,(void*)tmpstr);

	PRINTF("\r\n=======================================\r\n");
	PRINTF(" WIZnet %s EVB Demos v%d.%.2d\r\n", tmpstr, VER_H, VER_L);
	PRINTF("=======================================\r\n");
	PRINTF(">> TCP server and UDP loopback test\r\n");
	PRINTF(">> LED Blinky using SysTick: [SW1] On/Off\r\n");
	PRINTF("=======================================\r\n");

	Display_Net_Conf(); // Print out the network information to serial terminal
}


static void Net_Conf()
{
	/* wizchip netconf */
	ctlnetwork(CN_SET_NETINFO, (void*) &gWIZNETINFO);
}

#define SOCK_DHCP      		6
#define SOCK_TCPS     		0
#define LPBACK_SOCK_TCPS	1
#define PORT_TCPS			7777
#define MQTT_PORT_TCPS		7778
#define CLIENT_PORT_TCPS	1883
#define SOCK_DNS       		6
#define DATA_BUF_SIZE   	2048
uint8_t gDATABUF[DATA_BUF_SIZE];

uint8_t	sAddr[4]	  = {139, 227, 1, 243};//{106,15,33,159};//{10, 0, 0, 76}//{139,226,4,198};
uint8_t DNS_2nd[4]    = {10, 0, 0, 1};      	// Secondary DNS server IP  210.22.70.3{208, 67, 222, 222

uint32_t dns_run(uint8_t* Domain_name)
{
	//uint8_t Domain_name[] = "10.0.0.76";//"www.baidu.com";iot.eclipse.org    		// for Example domain name
	uint8_t Domain_IP[4]  = {0, };               		// Translated IP address by DNS Server

	int32_t	ret;
	/* DNS processing */
	if ((ret = DNS_run(gWIZNETINFO.dns, Domain_name, Domain_IP)) > 0) // try to 1st DNS
	{
	   printf("> 1st DNS Respond\r\n");
	}
	else if ((ret != -1) && ((ret = DNS_run(DNS_2nd, Domain_name, Domain_IP))>0))     // retry to 2nd DNS
	{
	   printf("> 2nd DNS Respond\r\n");
	}
	else if(ret == -1)
	{
	   printf("> MAX_DOMAIN_NAME is too small. Should be redefined it.\r\n");
	}
	else
	{
	   printf("> DNS Failed\r\n");
	}

	if(ret > 0)
	{
	   printf("> Translated %s to [%d.%d.%d.%d]\r\n",Domain_name,Domain_IP[0],Domain_IP[1],Domain_IP[2],Domain_IP[3]);
	   //
	   // TODO: To be executed User's code after a successful DNS process
	   //
	}

	return *(uint32_t*)Domain_IP;
}

void my_ip_assign(void)
{
   getIPfromDHCP(gWIZNETINFO.ip);
   getGWfromDHCP(gWIZNETINFO.gw);
   getSNfromDHCP(gWIZNETINFO.sn);
   getDNSfromDHCP(gWIZNETINFO.dns);
   gWIZNETINFO.dhcp = NETINFO_DHCP;
   /* Network initialization */
   Net_Conf();      // apply from DHCP

   Display_Net_Conf();
   printf("DHCP LEASED TIME : %ld Sec.\r\n", getDHCPLeasetime());
   printf("\r\n");
}

void my_ip_conflict(void)
{
	printf("CONFLICT IP from DHCP\r\n");
   //halt or reset or any...
   while(1); // this example is halt.
}

uint32_t temp_dns_addr;
void master_task(void *pvParameters)
{
	ETH_CS_INIT(1);
	ETH_nRST_INIT(1);
	SPI_Init();
	W5500_Init();
	Net_Conf();
	link_debug();

    /* Create the software timer. */
	Dhcp1sTimerHandle = xTimerCreate("Dhcp1sTimer",          /* Text name. */
								(1000 / portTICK_PERIOD_MS), /* Timer period. */
                                 pdTRUE,             /* Enable auto reload. */
                                 0,                  /* ID is not used. */
								 Dhcp1sTimerCallback);   /* The callback function. */
	xTimerStart(Dhcp1sTimerHandle, 0);
	DHCP_init(SOCK_DHCP, gDATABUF);
	reg_dhcp_cbfunc(my_ip_assign, my_ip_assign, my_ip_conflict);
	bool run_user_applications = false;
	int32_t ret;
	while(1)
	{
		switch(DHCP_run())
		{
			case DHCP_IP_ASSIGN:
			case DHCP_IP_CHANGED:
				/* If this block empty, act with default_ip_assign & default_ip_update */
				//
				// This example calls my_ip_assign in the two case.
				//
				// Add to ...
				//
				break;
			case DHCP_IP_LEASED:
				//
				// TODO: insert user's code here
				run_user_applications = true;
				//
				PRINTF("LEASED");
				break;
			case DHCP_FAILED:
				/* ===== Example pseudo code =====  */
				// The below code can be replaced your code or omitted.
				// if omitted, retry to process DHCP
				my_dhcp_retry++;
				if(my_dhcp_retry > MY_MAX_DHCP_RETRY)
				{
					gWIZNETINFO.dhcp = NETINFO_STATIC;
					DHCP_stop();      // if restart, recall DHCP_init()
					printf(">> DHCP %d Failed\r\n", my_dhcp_retry);
					Net_Conf();
					Display_Net_Conf();   // print out static netinfo to serial
					my_dhcp_retry = 0;
				}
				break;
			default:
				break;
		}

		if(run_user_applications)
		{
			// Loopback test : TCP Server
//			if ((ret = loopback_tcps(SOCK_TCPS, gDATABUF, PORT_TCPS)) < 0) // TCP server loopback test
//			{
//				printf("SOCKET ERROR : %ld\r\n", ret);
//			}
			break;
		} // End of user's code
	}

	DNS_init(SOCK_DNS, gDATABUF);
	//temp_dns_addr = dns_run();
extern void rtc_init(void);
	rtc_init();

//#include "MQTTFreeRTOS.h"
//	Network abc;
//
//	NetworkInit(&abc);
//	NetworkConnect(&abc, (char*)sAddr, CLIENT_PORT_TCPS);
//
//	vTaskDelay(1000);
//	FreeRTOS_disconnect(&abc);

	while(1)
	{
		if(run_user_applications)
		{
			//loopback_tcpc(2, gDATABUF, sAddr, CLIENT_PORT_TCPS);
			vTaskDelay(1000);
		} // End of user's code
	}

	vTaskSuspend(NULL);
}

