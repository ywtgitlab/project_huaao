/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BOARD_H_
#define _BOARD_H_

#include "clock_config.h"
#include "fsl_gpio.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*! @brief The board name */
#define BOARD_NAME "FRDM-K22F"

/*! @brief The UART to use for debug messages. */
#define BOARD_USE_UART
#define BOARD_DEBUG_UART_TYPE DEBUG_CONSOLE_DEVICE_TYPE_UART
#define BOARD_DEBUG_UART_BASEADDR (uint32_t) UART1
#define BOARD_DEBUG_UART_CLKSRC UART1_CLK_SRC
#define BOARD_DEBUG_UART_CLK_FREQ CLOCK_GetCoreSysClkFreq()
#define BOARD_UART_IRQ UART1_RX_TX_IRQn
#define BOARD_UART_IRQ_HANDLER UART1_RX_TX_IRQHandler

#ifndef BOARD_DEBUG_UART_BAUDRATE
#define BOARD_DEBUG_UART_BAUDRATE 115200
#endif /* BOARD_DEBUG_UART_BAUDRATE */


#define SHELL_UART 				UART1
#define SHELL_UART_CLKSRC 		UART1_CLK_SRC
#define SHELL_UART_RX_TX_IRQn 	UART1_RX_TX_IRQn

#define RF_UART 				UART0
#define RF_UART_CLKSRC 			UART0_CLK_SRC
#define RF_UART_RX_TX_IRQn 		UART0_RX_TX_IRQn


#define LOGIC_HIGH			1
#define LOGIC_LOW			0

#ifdef FREEDOM
#define ETH_CS_GPIO			GPIOD
#define ETH_CS_PIN			4U
#define ETH_nRST_GPIO		GPIOC
#define ETH_nRST_PIN		11U
#else
#define ETH_CS_GPIO			GPIOC
#define ETH_CS_PIN			4U
#define ETH_nRST_GPIO		GPIOC
#define ETH_nRST_PIN		1U
#define LED_RED_GPIO		GPIOA
#define LED_RED_PIN			5U
#define LED_GREEN_GPIO		GPIOA
#define LED_GREEN_PIN		12U
#define LED_BLUE_GPIO		GPIOA
#define LED_BLUE_PIN		13U
#define LED_SDN_GPIO		GPIOC
#define LED_SDN_PIN			9U
#define LED_LED1_GPIO		GPIOC
#define LED_LED1_PIN		10U
#define LED_LED2_GPIO		GPIOC
#define LED_LED2_PIN		11U
#endif

#define ETH_CS_INIT(output) 	GPIO_WritePinOutput(ETH_CS_GPIO, ETH_CS_PIN, output);	ETH_CS_GPIO->PDDR |= (1U << ETH_CS_PIN)
#define ETH_CS_SELECT()  	 	GPIO_ClearPinsOutput(ETH_CS_GPIO, 1U << ETH_CS_PIN) /*!< Turn on target LED_RED */
#define ETH_CS_DESELECT()  		GPIO_SetPinsOutput(ETH_CS_GPIO, 1U << ETH_CS_PIN) /*!< Turn off target LED_RED */

#define ETH_nRST_INIT(output) 	GPIO_WritePinOutput(ETH_nRST_GPIO, ETH_nRST_PIN, output);	ETH_nRST_GPIO->PDDR |= (1U << ETH_nRST_PIN)
#define ETH_nRST_SELECT()  	 	GPIO_ClearPinsOutput(ETH_nRST_GPIO, 1U << ETH_nRST_PIN) /*!< Turn on target LED_RED */
#define ETH_nRST_DESELECT()  	GPIO_SetPinsOutput(ETH_nRST_GPIO, 1U << ETH_nRST_PIN) /*!< Turn off target LED_RED */

#define LED_RED_INIT() 			GPIO_WritePinOutput(LED_RED_GPIO, LED_RED_PIN, LOGIC_HIGH);	LED_RED_GPIO->PDDR |= (1U << LED_RED_PIN)
#define LED_RED_ON()	  	 	GPIO_ClearPinsOutput(LED_RED_GPIO, 1U << LED_RED_PIN) /*!< Turn on target LED_RED */
#define LED_RED_OFF()		  	GPIO_SetPinsOutput(LED_RED_GPIO, 1U << LED_RED_PIN) /*!< Turn off target LED_RED */

#define LED_GREEN_INIT() 		GPIO_WritePinOutput(LED_GREEN_GPIO, LED_GREEN_PIN, LOGIC_HIGH);	LED_GREEN_GPIO->PDDR |= (1U << LED_GREEN_PIN)
#define LED_GREEN_ON()  		GPIO_ClearPinsOutput(LED_GREEN_GPIO, 1U << LED_GREEN_PIN) /*!< Turn on target LED_RED */
#define LED_GREEN_OFF()  		GPIO_SetPinsOutput(LED_GREEN_GPIO, 1U << LED_GREEN_PIN) /*!< Turn off target LED_RED */

#define LED_BLUE_INIT() 		GPIO_WritePinOutput(LED_BLUE_GPIO, LED_BLUE_PIN, LOGIC_HIGH);	LED_BLUE_GPIO->PDDR |= (1U << LED_BLUE_PIN)
#define LED_BLUE_ON()  	 		GPIO_ClearPinsOutput(LED_BLUE_GPIO, 1U << LED_BLUE_PIN) /*!< Turn on target LED_RED */
#define LED_BLUE_OFF()  		GPIO_SetPinsOutput(LED_BLUE_GPIO, 1U << LED_BLUE_PIN) /*!< Turn off target LED_RED */

#define LED_SDN_INIT() 			GPIO_WritePinOutput(LED_SDN_GPIO, LED_SDN_PIN, LOGIC_HIGH);	LED_SDN_GPIO->PDDR |= (1U << LED_SDN_PIN)
#define LED_SDN_ON()  	 		GPIO_ClearPinsOutput(LED_SDN_GPIO, 1U << LED_SDN_PIN) /*!< Turn on target LED_RED */
#define LED_SDN_OFF()  			GPIO_SetPinsOutput(LED_SDN_GPIO, 1U << LED_SDN_PIN) /*!< Turn off target LED_RED */

#define LED_LED1_INIT() 		GPIO_WritePinOutput(LED_LED1_GPIO, LED_LED1_PIN, LOGIC_HIGH);	LED_LED1_GPIO->PDDR |= (1U << LED_LED1_PIN)
#define LED_LED1_ON()  	 		GPIO_ClearPinsOutput(LED_LED1_GPIO, 1U << LED_LED1_PIN) /*!< Turn on target LED_RED */
#define LED_LED1_OFF()  		GPIO_SetPinsOutput(LED_LED1_GPIO, 1U << LED_LED1_PIN) /*!< Turn off target LED_RED */

#define LED_LED2_INIT() 		GPIO_WritePinOutput(LED_LED2_GPIO, LED_LED2_PIN, LOGIC_HIGH);	LED_LED2_GPIO->PDDR |= (1U << LED_LED2_PIN)
#define LED_LED2_ON()  	 		GPIO_ClearPinsOutput(LED_LED2_GPIO, 1U << LED_LED2_PIN) /*!< Turn on target LED_RED */
#define LED_LED2_OFF()  		GPIO_SetPinsOutput(LED_LED2_GPIO, 1U << LED_LED2_PIN) /*!< Turn off target LED_RED */


/* @brief The SDSPI disk PHY configuration. */
#define BOARD_SDSPI_SPI_BASE SPI0_BASE /*!< SPI base address for SDSPI */
#define BOARD_SDSPI_CD_GPIO_BASE GPIOB /*!< Port related to card detect pin for SDSPI */
#define BOARD_SDSPI_CD_PIN 16U         /*!< Card detect pin for SDSPI */
#define BOARD_SDSPI_CD_LOGIC_RISING    /*!< Logic of card detect pin for SDSPI */


#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
 * API
 ******************************************************************************/

void BOARD_InitDebugConsole(void);
void ms_delay(uint32_t msec);
void us_delay(uint32_t usec);

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* _BOARD_H_ */
