/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "fsl_uart_freertos.h"
#include "fsl_uart.h"

#include "pin_mux.h"
#include "clock_config.h"

#include "common.h"
#include "rf_interface.h"

uart_rtos_handle_t rf_handle;
struct _uart_handle rf_t_handle;

uint8_t rf_background_buffer[512];
uint8_t rf_recv_buffer[4];

uart_rtos_config_t rf_uart_config = {
    .baudrate = 115200,
    .parity = kUART_ParityDisabled,
    .stopbits = kUART_OneStopBit,
    .buffer = rf_background_buffer,
    .buffer_size = sizeof(rf_background_buffer),
};

node_data_protocol_head_t NDPHead;

void rf_send_zuwang(void)
{
	uint16_t 	crc16;
	uint8_t 	frame_buf[1024];
	NDPHead.SyncWord = SYNCWORD;
	NDPHead.Command  = reqFreeNetWorking;
	NDPHead.DataLen  = 0;
	memcpy(frame_buf, &NDPHead, 7);

	crc16 = slow_crc16(0x33, &NDPHead, 7);
	memcpy(frame_buf+7, (uint8_t *)&crc16, 2);

	UART_RTOS_Send(&rf_handle, (uint8_t *)rf_recv_buffer, 4);
}

DataStatus_Single_t recv_data_single;

void rf_uart_task(void *pvParameters)
{
    int error;
    size_t n, data_len;
    uint8_t rf_data_buffer[512];

    rf_uart_config.srcclk = CLOCK_GetFreq(RF_UART_CLKSRC);
    rf_uart_config.base   = RF_UART;
    NVIC_SetPriority(RF_UART_RX_TX_IRQn, 6);

    if (0 > UART_RTOS_Init(&rf_handle, &rf_t_handle, &rf_uart_config))
    {
        vTaskSuspend(NULL);
    }

    do
    {
        error = UART_RTOS_Receive(&rf_handle, rf_recv_buffer, sizeof(rf_recv_buffer), &n);
        if (error == kStatus_UART_RxHardwareOverrun)
        {
            /* Notify about hardware buffer overrun */
        	PRINTF("rf nHardware buffer overrun!\r\n");
        }
        if (error == kStatus_UART_RxRingBufferOverrun)
        {
            /* Notify about ring buffer overrun */
        	PRINTF("rf Ring buffer overrun!\r\n");
        }
        if (n > 0)
        {
        	for (int i=0; i<4; i++)
        	{
        		PRINTF("%02x ", rf_recv_buffer[i]);
        	}
        	if (SYNCWORD == *(uint32_t*)rf_recv_buffer)
        	{
        		UART_RTOS_Receive(&rf_handle, rf_recv_buffer, 1, &n);
        		PRINTF("%02x ", rf_recv_buffer[0]);
        		if ( 0x6F == rf_recv_buffer[0])
        		{
        			UART_RTOS_Receive(&rf_handle, rf_recv_buffer, 2, &n);
        			data_len = *(uint16_t*)rf_recv_buffer;
        			//PRINTF("%04x ", data_len);
                	for (int i=0; i<n; i++)
                	{
                		PRINTF("%02x ", rf_recv_buffer[i]);
                	}

        			UART_RTOS_Receive(&rf_handle, rf_data_buffer, data_len+2, &n);
                	for (int i=0; i<n; i++)
                	{
                		PRINTF("%02x ", rf_data_buffer[i]);
                	}
                	PRINTF("\r\n");
                	for (int j=0; j < n/sizeof(recv_data_single); j++)
                	{

                		memcpy(&recv_data_single, rf_data_buffer + sizeof(recv_data_single)*j, sizeof(recv_data_single));
                		create_objects_rf(&recv_data_single);
                	}
        		}
        	}
        	PRINTF("\r\n");
        }
        //vTaskDelay(1000);
    } while (1);//kStatus_Success == error

}
