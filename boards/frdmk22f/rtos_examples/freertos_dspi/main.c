/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "fsl_common.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MQTTFreeRTOS.h"

//void SPI_Init();
//void W5500_Init();
void cjson_test_proc(void *);
void master_task(void *);
void shell_task(void *);
void rf_uart_task(void *pvParameters);
extern void prvMQTTEchoTask(void *);

int main(void)
{
    /* Init board hardware. */
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

    send_xSemaphore = xSemaphoreCreateMutex();
    recv_xSemaphore = xSemaphoreCreateMutex();

    PRINTF("\r\nstart sys\r\n");
    gpio_init();

    if (xTaskCreate(master_task, "Master_task", configMINIMAL_STACK_SIZE * 10, NULL, 7, NULL) != pdPASS)
    {
        PRINTF("Failed to create master task");
        vTaskSuspend(NULL);
    }

    if (xTaskCreate(prvMQTTEchoTask, "MQTTEcho", configMINIMAL_STACK_SIZE * 20, NULL, 6, NULL)!= pdPASS)
    {
		PRINTF("Failed to create MQTTEcho task");
		vTaskSuspend(NULL);
    };

//    if (xTaskCreate(cjson_test_proc, "Cjson_test_proc", configMINIMAL_STACK_SIZE * 10, NULL, 3, NULL) != pdPASS)
//    {
//        PRINTF("Failed to create Rf_uart task");
//        vTaskSuspend(NULL);
//    }

    if (xTaskCreate(rf_uart_task, "Rf_uart_task", configMINIMAL_STACK_SIZE * 20, NULL, 2, NULL) != pdPASS)
    {
        PRINTF("Failed to create Rf_uart task");
        vTaskSuspend(NULL);
    }

    if (xTaskCreate(shell_task, "Shell_task", configMINIMAL_STACK_SIZE * 4, NULL, 1, NULL) != pdPASS)
    {
        PRINTF("Failed to create shell task");
        vTaskSuspend(NULL);
    }

    /* Start scheduling. */
    vTaskStartScheduler();
    for (;;)
        ;

}

