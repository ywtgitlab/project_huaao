/* Copyright (c) 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "event_groups.h"
#include "semphr.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "board.h"
#include "fsl_shell.h"
#include "fsl_debug_console.h"

#include "fsl_uart_freertos.h"

#include "fsl_common.h"
#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SHELL_Printf PRINTF


/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void Led_Init(void);

/* SHELL user send data callback */
void SHELL_SendDataCallback(uint8_t *buf, uint32_t len);

/* SHELL user receive data callback */
void SHELL_ReceiveDataCallback(uint8_t *buf, uint32_t len);

static int32_t LedControl(p_shell_context_t context, int32_t argc, char **argv);

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint8_t shell_background_buffer[256];

uart_rtos_handle_t 	shell_handle;
struct _uart_handle shell_t_handle;

uart_rtos_config_t 	shell_uart_config = {
    .baudrate = BOARD_DEBUG_UART_BAUDRATE,
    .parity = kUART_ParityDisabled,
    .stopbits = kUART_OneStopBit,
    .buffer = shell_background_buffer,
    .buffer_size = sizeof(shell_background_buffer),
};


static const shell_command_context_t xLedCommand = {"led",
                                                    "\r\n\"led arg1 arg2\":\r\n Usage:\r\n    arg1: 1|2|3|4...         "
                                                    "   Led index\r\n    arg2: on|off                Led status\r\n",
                                                    LedControl, 2};

/*******************************************************************************
 * Code
 ******************************************************************************/


void SHELL_SendDataCallback(uint8_t *buf, uint32_t len)
{
	UART_RTOS_Send(&shell_handle, buf, len);
//    while (len--)
//    {
//        PUTCHAR(*(buf++));
//    }
}

void SHELL_ReceiveDataCallback(uint8_t *buf, uint32_t len)
{
	size_t n;
	UART_RTOS_Receive(&shell_handle, buf, len, &n);
	len = n;
//    while (len--)
//    {
//        *(buf++) = GETCHAR();
//    }
}

static int32_t LedControl(p_shell_context_t context, int32_t argc, char **argv)
{
	return 0;
}

/*! @brief Main function */
void shell_task(void *u)
{
    shell_context_struct user_context;

    shell_uart_config.srcclk = CLOCK_GetFreq(SHELL_UART_CLKSRC);
    shell_uart_config.base = SHELL_UART;
    NVIC_SetPriority(SHELL_UART_RX_TX_IRQn, 8);
    if (0 > UART_RTOS_Init(&shell_handle, &shell_t_handle, &shell_uart_config))
    {
        vTaskSuspend(NULL);
    }

    /* Init SHELL */
    SHELL_Init(&user_context, SHELL_SendDataCallback, SHELL_ReceiveDataCallback, SHELL_Printf, ">> ");

    /* Add new command to commands list */
    SHELL_RegisterCommand(&xLedCommand);

    SHELL_Main(&user_context);

}
