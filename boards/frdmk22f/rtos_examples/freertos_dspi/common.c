
#include "common.h"

#include "fsl_debug_console.h"
#include "fsl_rtc.h"
#include "sntp.h"
#include "dns.h"

uint8_t g_sntp_buf[128];
datetime g_sntp_datatime;

/*
 * Hash��Ϊ0x33
 * e.g. slow_crc16(0x33, (u8*)&aucUartFrame, BYTE_OF_FRAME);
 */
unsigned short slow_crc16(unsigned short sum, unsigned char *p, unsigned int len)
{
	while (len--)
	{
		int i;
		unsigned char byte = *p++;
		for (i = 0; i < 8; ++i)
		{
			unsigned long osum = sum;
			sum <<= 1;
			if (byte & 0x80)
			  sum |= 1 ;
			if (osum & 0x8000)
			  sum ^= 0x1021;
			byte <<= 1;
		}
	}
	return sum;
}

void BOARD_SetRtcClockSource(void)
{
    /* Enable the RTC 32KHz oscillator */
    RTC->CR |= RTC_CR_OSCE_MASK;
}

void rtc_init(void)
{
    uint32_t sec;
    uint32_t currSeconds;
    rtc_datetime_t date;
    rtc_config_t rtcConfig;
    uint32_t	entry_tick = xTaskGetTickCount();
    //printf("%d\r\n", xTaskGetTickCount());

	uint8_t	sntp_sAddr[4];
	int8_t  sntp_ret = 0;

    RTC_GetDefaultConfig(&rtcConfig);
    RTC_Init(RTC, &rtcConfig);
    /* Select RTC clock source */
    BOARD_SetRtcClockSource();

    /* RTC time counter has to be stopped before setting the date & time in the TSR register */
    RTC_StopTimer(RTC);

    *(uint32_t*)sntp_sAddr = dns_run("time.nist.gov");
	SNTP_init(1, sntp_sAddr, 21, g_sntp_buf);
	while( (xTaskGetTickCount() - entry_tick) < 5000)
	{
		sntp_ret = SNTP_run(&g_sntp_datatime);
		if (sntp_ret == 1)
		{
			memcpy(&date, &g_sntp_datatime, sizeof(rtc_datetime_t));
			/* Set RTC time */
			RTC_SetDatetime(RTC, &date);
			break;
		}
	}
	if (1 != sntp_ret) NVIC_SystemReset();
	RTC_StartTimer(RTC);

    RTC_GetDatetime(RTC, &date);

    printf("Current datetime: %04hd-%02hd-%02hdT%02hd:%02hd:%02hd.000Z\r\n",
           date.year,
           date.month,
           date.day,
           date.hour,
           date.minute,
           date.second);

}
