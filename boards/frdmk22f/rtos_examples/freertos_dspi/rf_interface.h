#ifndef _RF_INTERFACE_H
#define _RF_INTERFACE_H


#define	SYNCWORD	0xaa55aa55


#define reqFreeNetWorking	0x02
#define reqDownXY			0x07
#define	reqNodeData			0x0f
#define reqNodeDataErr		0x47

#define rspDownXY			0x67
#define rspNodeData 		0x6f



//enum {
//	NoneAck	= 0x00,
//	CmdErr	= 0x01,
//	CmdBusy	= 0x02,
//	CmdNoAnalysis,
//	NodeLost,
//	NoClearResult,
//	CmdNoArrived,
//	SysBusy,
//	CrcOk,
//	CrcErr,
//
//}CommandReq_t;

enum {
	NoneAck	= 0x00,
	CmdErr	= 0x01,
	CmdBusy	= 0x02,
	CmdNoAnalysis,
	NodeLost,
	NoClearResult,
	CmdNoArrived,
	SysBusy,
	CrcOk,
	CrcErr,

}CommandRsp_t;

typedef struct node_data_protocol{
	uint32_t SyncWord;	// 仅仅使用24位
	uint8_t  Command;	//仅仅使用12位
	uint16_t DataLen;	//仅仅使用12位

}node_data_protocol_head_t;

//	uint8_t* DataPkt;
//	uint16_t Crc;		//CMD~DATA(N)

typedef struct node_pos{
	uint32_t Id:24;	// 仅仅使用24位
	uint16_t x:12;	//仅仅使用12位
	uint16_t y:12;	//仅仅使用12位
}node_pos_t;

typedef struct node_group{
	uint8_t 	ucGroupLevel;
	uint32_t 	ulParentId :24; // 仅仅使用24位
	uint32_t 	ulSelfId :24; // 仅仅使用24位
	uint16_t 	usNumberOfChildMembers;
	uint8_t* 	paucChildMembersId[3]; // 相当于aucMembersId[usNumberOfgroupMembers][3],存放的为子节点的id号
}node_group_t;

typedef struct Node_Add
{
	uint32_t 	ulParentId :24; // 仅仅使用24位，表示这些节点加入到该节点子节点集合中
	uint16_t 	usNumberOfAddMembers;
	uint8_t* 	paucAddMembersId[3]; // 相当于aucAddMembersId [usNumberOfAddMembers][3],存放的为丢失的节点的id号
}Node_Add_t;

typedef struct Lostnode_group
{
	uint16_t usNumberOfLostMembers;
	uint8_t* paucLostMembersId[3]; // 相当于aucLostMembersId [usNumberOfLostMembers][3],存放的为丢失的节点的id号
}Lostnode_group_t;

typedef struct Command_For_Overall
{
	uint8_t tCommandContent;
}Command_For_Overall_t;

typedef struct Command_For_Group
{
	uint8_t tCommandContent;
	uint8_t ucGroupLevel;
}Command_For_Group_t;

typedef struct Command_For_Single
{
	uint32_t 	ulNodeId :24; // 仅仅使用24位
	uint8_t 	tCommandContent;
}Command_For_Single_t;

typedef struct DataStatus_Single
{
	uint32_t ulNodeId :24; // 仅仅使用24位
	uint16_t usVin;   // x 100
	uint16_t usVout;  // x 100
	uint16_t usIin;   // x 100
	uint16_t usTemperature; // x 100
	uint16_t usAlarmMask;   // If set, then alarm = true,
	uint16_t usPowerWatt;   // x 100
	uint16_t usDataValid;
	uint8_t  ucReserved2;
	uint8_t  pad;
}DataStatus_Single_t; // DataBlockn

//typedef struct DataStatus_Single
//{
//	uint16_t usVin;   // x 100
//	uint16_t usVout;  // x 100
//	uint16_t usIin;   // x 100
//	uint16_t usTemperature; // x 100
//	uint16_t usAlarmMask;   // If set, then alarm = true,
//	uint16_t usPowerWatt;   // x 100
//	uint16_t usStatus;
//}DataStatus_Single_t; // DataBlockn

typedef struct Numbers
{
	uint8_t  Partnumber[10];
	uint8_t  SWVersion[10];
}Numbers_t;

typedef struct Status
{
	uint16_t usStatus;
}Status_t;



#endif
