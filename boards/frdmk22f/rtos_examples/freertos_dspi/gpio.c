
#include "board.h"






void gpio_init(void)
{
    LED_RED_INIT();
    LED_GREEN_INIT();
	LED_BLUE_INIT();
	LED_SDN_INIT();
	LED_LED1_INIT();
	LED_LED2_INIT();

    LED_RED_ON();
    ms_delay(200);
	LED_GREEN_ON();
	ms_delay(200);
	LED_BLUE_ON();
	ms_delay(200);
	LED_SDN_ON();
	ms_delay(200);
	LED_LED1_ON();
	ms_delay(200);
	LED_LED2_ON();
	ms_delay(200);

    LED_RED_OFF();
	LED_GREEN_OFF();
	LED_BLUE_OFF();
	LED_SDN_OFF();
	LED_LED1_OFF();
	LED_LED2_OFF();
}
