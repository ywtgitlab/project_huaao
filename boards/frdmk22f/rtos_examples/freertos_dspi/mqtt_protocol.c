//Include: W5500 iolibrary
#include "w5500.h"
#include "wizchip_conf.h"

#include "mqtt_interface.h"

//Include: Internet iolibrary
#include "MQTTClient.h"

//Include: Standard IO Library
#include <stdio.h>
#include <string.h>

//Socket number defines
#define TCP_SOCKET	0

//Receive Buffer Size define
#define BUFFER_SIZE	2048

//Global variables
unsigned char targetIP[4] = {}; // mqtt server IP

unsigned char tempBuffer[BUFFER_SIZE] = {};

typedef struct opts_struct
{
	char* clientid;
	int nodelimiter;
	char* delimiter;
	enum QoS qos;
	char* username;
	char* password;
	char* host;
	int port;
	int showtopics;
}opts_mqtt;

opts_mqtt opts={	//opts;
		.clientid = (char*)"stdout-subscriber",
		.nodelimiter = 0,
		.delimiter = (char*)"\n",
		.qos = QOS0,
		.username = NULL,
		.password = NULL,
		.host = (char*)targetIP,
		.port = 1833,
		.showtopics = 0 };

// @brief messageArrived callback function
void messageArrived(MessageData* md)
{
	unsigned char testbuffer[100];
	MQTTMessage* message = md->message;

	if (opts.showtopics)
	{
		memcpy(testbuffer,(char*)message->payload,(int)message->payloadlen);
		*(testbuffer + (int)message->payloadlen + 1) = "\n";
		printf("%s\r\n",testbuffer);
	}

	if (opts.nodelimiter)
		printf("%.*s", (int)message->payloadlen, (char*)message->payload);
	else
		printf("%.*s%s", (int)message->payloadlen, (char*)message->payload, opts.delimiter);
}


int mqttmain(void)
{
	int rc = 0;
	unsigned char buf[100];



	Network n;
	MQTTClient c;

	NewNetwork(&n, TCP_SOCKET);
//	ConnectNetwork(&n, targetIP, targetPort);
	MQTTClientInit(&c,&n,1000,buf,100,tempBuffer,2048);

	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	data.willFlag = 0;
	data.MQTTVersion = 3;
	data.clientID.cstring = opts.clientid;
	data.username.cstring = opts.username;
	data.password.cstring = opts.password;

	data.keepAliveInterval = 60;
	data.cleansession = 1;

	rc = MQTTConnect(&c, &data);
	printf("Connected %d\r\n", rc);
	opts.showtopics = 1;

	printf("Subscribing to %s\r\n", "hello/wiznet");
	rc = MQTTSubscribe(&c, "hello/wiznet", opts.qos, messageArrived);
	printf("Subscribed %d\r\n", rc);

    while(1)
    {
    	MQTTYield(&c, data.keepAliveInterval);
    }
}
