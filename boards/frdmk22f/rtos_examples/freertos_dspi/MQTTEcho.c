/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "event_groups.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

#include "MQTTClient.h"

uint8_t payload[200];
extern char mqtt_out[512];
extern char rf_data_flag;

#define B0 (1 << 0)
static EventGroupHandle_t publish_event_group = NULL;
TimerHandle_t 			Publish1sTimerHandle = NULL;
TimerHandle_t 			Publish1sTimerHandleOther = NULL;


void cjson_data_ready(void)
{
	xEventGroupSetBits(publish_event_group, B0);
}

static void Publish1sTimerCallback(void *pvParameters)
{
	//create_objects_example();
	xEventGroupSetBits(publish_event_group, B0);
}

static void Publish1sTimerCallbackOther(void *pvParameters)
{
	//create_objects_example();
	xEventGroupSetBits(publish_event_group, B0);
}


void messageArrived(MessageData* data)
{
	printf("Message arrived on topic %.*s: %.*s\r\n", data->topicName->lenstring.len, data->topicName->lenstring.data,
		data->message->payloadlen, data->message->payload);
}

void prvMQTTEchoTask(void *pvParameters)
{
	/* connect to m2m.eclipse.org, subscribe to a topic, send and receive messages regularly every 1 sec */
	MQTTClient client;
	Network network;
	unsigned char sendbuf[512], readbuf[512];
	int rc = 0, count = 0;
	MQTTPacket_connectData connectData = MQTTPacket_connectData_initializer;
	pvParameters = 0;

	publish_event_group = xEventGroupCreate();
    /* Create the software timer. */
//	Publish1sTimerHandle = xTimerCreate("Publish1sTimer",          /* Text name. */
//								(1000 / portTICK_PERIOD_MS), /* Timer period. */
//                                 pdTRUE,             /* Enable auto reload. */
//                                 0,                  /* ID is not used. */
//								 Publish1sTimerCallback);   /* The callback function. */
//	xTimerStart(Publish1sTimerHandle, 0);

//    /* Create the software timer. */
//	Publish1sTimerHandleOther = xTimerCreate("Publish1sTimerOther",          /* Text name. */
//								(777 / portTICK_PERIOD_MS), /* Timer period. */
//                                 pdTRUE,             /* Enable auto reload. */
//                                 0,                  /* ID is not used. */
//								 Publish1sTimerCallbackOther);   /* The callback function. */
//	xTimerStart(Publish1sTimerHandleOther, 0);

	NetworkInit(&network);
	MQTTClientInit(&client, &network, 60000, sendbuf, sizeof(sendbuf), readbuf, sizeof(readbuf));

	char* address = "iot.eclipse.org";//"ywt.iot.eclipse.org";//106.15.33.159";"10.0.0.76";"iot.eclipse.org";139.227.1.243
	if ((rc = NetworkConnect(&network, address, 1883)) != 0)//1883
		printf("Return code from network connect is %d\r\n", rc);

#if defined(MQTT_TASK)
	if ((rc = MQTTStartTask(&client)) != pdPASS)
		printf("Return code from start tasks is %d\r\n", rc);
#endif

	connectData.MQTTVersion = 3;
	connectData.clientID.cstring = "FreeRTOS_sample212314";//FreeRTOS_sample

	if ((rc = MQTTConnect(&client, &connectData)) != 0)
		printf("Return code from MQTT connect is %d\r\n", rc);
	else
		printf("MQTT Connected\n");

//	if ((rc = MQTTSubscribe(&client, "FreeRTOS/sample/#", 2, messageArrived)) != 0)//FreeRTOS/sample/#
//		printf("Return code from MQTT subscribe is %d\r\n", rc);

	while (++count)
	{
//		create_objects_example();
//		vTaskDelay(3000);
        xEventGroupWaitBits(publish_event_group,    /* The event group handle. */
                                         B0,        /* The bit pattern the event group is waiting for. */
                                         pdTRUE,         /* BIT_0 and BIT_4 will be cleared automatically. */
										 pdTRUE,        /* Don't wait for both bits, either bit unblock task. */
                                         portMAX_DELAY); /* Block indefinitely to wait for the condition to be met. */
        printf("MQTT publish\n");

//		if ((rc = MQTTConnect(&client, &connectData)) != 0)
//			printf("Return code from MQTT connect is %d\r\n", rc);
//		else
//			printf("MQTT Connected\n");
//		while (1)
//		{
//			if (rf_data_flag) break;
//			vTaskDelay(500);
//		}
		MQTTMessage message;
		message.qos = 1;
		message.retained = 0;
		message.payload = mqtt_out;//out  payload mqtt_out
//		sprintf(payload, "message number %d", count);
//		sprintf(payload, "{66665555111111111111111111111111111112222221113333333}; %d", count);
//		message.payloadlen = strlen(payload);//payload  mqtt_out
		message.payloadlen = strlen(mqtt_out);
		printf("%s\r\n", mqtt_out);
		if ((rc = MQTTPublish(&client, "FreeRTOS/sample/a", &message)) != 0)//FreeRTOS/sample/a  upload
			printf("Return code from MQTT publish is %d\r\n", rc);
#if !defined(MQTT_TASK)
		if ((rc = MQTTYield(&client, 1000)) != 0)
			printf("Return code from yield is %d\r\n", rc);
#endif

	}

	/* do not return */
}


//void vStartMQTTTasks(uint16_t usTaskStackSize, UBaseType_t uxTaskPriority)
//{
//	BaseType_t x = 0L;
//
//	xTaskCreate(prvMQTTEchoTask,	/* The function that implements the task. */
//			"MQTTEcho0",			/* Just a text name for the task to aid debugging. */
//			usTaskStackSize,	/* The stack size is defined in FreeRTOSIPConfig.h. */
//			(void *)x,		/* The task parameter, not used in this case. */
//			uxTaskPriority,		/* The priority assigned to the task is defined in FreeRTOSConfig.h. */
//			NULL);				/* The task handle is not used. */
//}
/*-----------------------------------------------------------*/


