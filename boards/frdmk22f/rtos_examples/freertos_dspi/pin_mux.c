/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * TEXT BELOW IS USED AS SETTING FOR THE PINS TOOL *****************************
PinsProfile:
- !!product 'Pins v1.0'
- !!processor 'MK22FN512xxx12'
- !!package 'MK22FN512VLH12'
- !!mcu_data 'ksdk2_0'
- !!processor_version '1.0.0'
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR THE PINS TOOL ***
 */

#include "fsl_common.h"
#include "fsl_port.h"
#include "pin_mux.h"

/*FUNCTION**********************************************************************
 *
 * Function Name : BOARD_InitPins
 * Description   : Configures pin routing and optionally pin electrical features.
 *
 *END**************************************************************************/
void BOARD_InitPins(void) {
	CLOCK_EnableClock(kCLOCK_PortA);                       	/* Port A Clock Gate Control: Clock enabled */
	CLOCK_EnableClock(kCLOCK_PortB);                       	/* Port A Clock Gate Control: Clock enabled */
	CLOCK_EnableClock(kCLOCK_PortC);                       	/* Port C Clock Gate Control: Clock enabled */
	CLOCK_EnableClock(kCLOCK_PortD);                      	/* Port D Clock Gate Control: Clock enabled */
	CLOCK_EnableClock(kCLOCK_PortE);                    	/* Port E Clock Gate Control: Clock enabled */

	PORT_SetPinMux(PORTE,  0u, kPORT_MuxAlt3);            	/* PORTE0 (pin 1) is configured as UART1_TX */
	PORT_SetPinMux(PORTE,  1u, kPORT_MuxAlt3);            	/* PORTE1 (pin 2) is configured as UART1_RX */

#ifdef Sundial
	/* for w5500 */
	PORT_SetPinMux(PORTC,  1u, kPORT_MuxAsGpio);			/* N_RESET */
	PORT_SetPinMux(PORTC,  4u, kPORT_MuxAsGpio);          	/* PORTC4 (pin 49) is configured as SPI0_PCS0 */
	PORT_SetPinMux(PORTC,  5u, kPORT_MuxAlt2);            	/* PORTC5 (pin 50) is configured as SPI0_SCK */
	PORT_SetPinMux(PORTC,  6u, kPORT_MuxAlt2);            	/* PORTC6 (pin 51) is configured as SPI0_SOUT */
	PORT_SetPinMux(PORTC,  7u, kPORT_MuxAlt2);            	/* PORTC7 (pin 52) is configured as SPI0_SIN */
	/* for sdcard */
	PORT_SetPinMux(PORTD,  0u, kPORT_MuxAsGpio);			//SD_ON/OFF
	PORT_SetPinMux(PORTD,  4u, kPORT_MuxAlt7);				/* SPI1_PCS0 */
	PORT_SetPinMux(PORTD,  5u, kPORT_MuxAlt7);            	/* SPI1_SCK */
	PORT_SetPinMux(PORTD,  6u, kPORT_MuxAlt7);            	/* SPI1_SOUT */
	PORT_SetPinMux(PORTD,  7u, kPORT_MuxAlt7);            	/* SPI1_SIN */
	/* led  */
	PORT_SetPinMux(PORTA,  5u, kPORT_MuxAsGpio);			//red
	PORT_SetPinMux(PORTA, 12u, kPORT_MuxAsGpio);			//green
	PORT_SetPinMux(PORTA, 13u, kPORT_MuxAsGpio);			//blue
	PORT_SetPinMux(PORTC,  9u, kPORT_MuxAsGpio);			//LED_shut down
	PORT_SetPinMux(PORTC, 10u, kPORT_MuxAsGpio);			//LED1
	PORT_SetPinMux(PORTC, 11u, kPORT_MuxAsGpio);			//LED2
	/* UART0 for rf */
	PORT_SetPinMux(PORTB, 16u, kPORT_MuxAlt3);            	/* UART0_RX */
	PORT_SetPinMux(PORTB, 17u, kPORT_MuxAlt3);            	/* UART0_TX */



#endif

#ifdef  FREEDOM
  PORT_SetPinMux(PORTC, 11u, kPORT_MuxAsGpio);			/* N_RESET */
  PORT_SetPinMux(PORTD,  4u, kPORT_MuxAlt7);            /* PORTC4 (pin 49) is configured as SPI1_PCS0 */
  PORT_SetPinMux(PORTD,  5u, kPORT_MuxAlt7);            /* PORTC5 (pin 50) is configured as SPI1_SCK */
  PORT_SetPinMux(PORTD,  6u, kPORT_MuxAlt7);            /* PORTC6 (pin 51) is configured as SPI1_SOUT */
  PORT_SetPinMux(PORTD,  7u, kPORT_MuxAlt7);            /* PORTC7 (pin 52) is configured as SPI1_SIN */

  //PORT_SetPinMux(PORTD, 4u, kPORT_MuxAlt7);            /* PORTD4 (pin 61) is configured as SPI0_PCS0 */
  PORT_SetPinMux(PORTD,  4u, kPORT_MuxAsGpio);
  PORT_SetPinMux(PORTD,  1u, kPORT_MuxAlt2);            /* PORTD5 (pin 62) is configured as SPI0_SCK */
  PORT_SetPinMux(PORTD,  2u, kPORT_MuxAlt2);            /* PORTD6 (pin 63) is configured as SPI0_SOUT */
  PORT_SetPinMux(PORTD,  3u, kPORT_MuxAlt2);            /* PORTD7 (pin 64) is configured as SPI0_SIN */
#endif

}

/*******************************************************************************
 * EOF
 ******************************************************************************/
